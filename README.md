**Briefing de web:**

A empresa **TheDogs** é um canil que deseja vender seus cachorros de raça online.

Para isso, essas são as funcionalidades: - Implementar a busca por nome das raças; - Listar os resultados da busca; - Exibir os detalhes do animal; - Adicionar o animal a um carrinho de compras sem necessidade de login; - Criar uma tela de checkout solicitando identificação básica do comprador (Nome, CPF e e-mail) e um endereço de entrega.
No checkout todos os dados da compra, raça, quantidade, valor, dados do comprador e endereço de entrega devem ser gravados no banco de dados.

Após o checkout o sistema deve retornar para a página inicial com o carrinho de compras "zerado".

1. Você deve usar algumas dessas tecnologias:
Ruby On Rails – PHP (CakePHP ou Laravel)

2. Você deve usar alguns desses bancos de dados:
MySQL  - PostgreSQL - MongoDB

3. Você deve escrever testes de unidade e integração. Não precisa ser para todas funcionalidades.
O que vamos avaliar:  Código limpo, cobertura de testes, design patterns, arquitetura, performance, segurança, dominio da tecnologia, entendimento do desafio.

Você pode deixar a sua aplicação em um ambiente cloud free (AWS, Heroku, Digital Ocean, Linode...) isso com certeza será um diferencial!

Você deve criar um repositório Git público no Github ou Bitbucket para conseguirmos analisar o seu código.
Você pode utilizar como front para sua a aplicação esse repositório https://bitbucket.org/grupofocusnetworks/teste_web_focus.
O prazo para conclusão dos desafios é de até 3 dias corridos à partir da data de entrega desse desafio. Quanto antes, melhor!

Vamos?